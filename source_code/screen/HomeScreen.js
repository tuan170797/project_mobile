import * as React from 'react'
import { View, Text, ScrollView, StyleSheet, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native';
import Image from 'react-native-scalable-image'
import { calcScale, mainBgr, currencyFormat } from './../constants/Constants'
import AntDesign from 'react-native-vector-icons/AntDesign'
import MainAPI from './../api/MainAPI'
import AsyncStorage from '@react-native-community/async-storage'
import { SliderBox } from 'react-native-image-slider-box'

class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            displayPopular: true,
            displayNew: false,
            displayPrice: false,
            listData: [],
            listCategories: [],
            categoryID: 0,
            isLoading: true,
            numberOfCartItems: 0,
            images: [
                require('./../image/homebanner.jpg'),
                require('./../image/homebanner2.jpg'),
                require('./../image/homebanner3.jpg'),
                require('./../image/homebanner4.jpg'),
                require('./../image/homebanner5.jpg'),
            ],
            searchKey: ''
        }
    }

    loadData = async () => {
        this.api = new MainAPI()
        const listCategories = await this.getCategories()
        const categoryID = listCategories[0].id
        const listData = await this.api.getPopularProducts(categoryID)
        this.setState({
            listCategories: listCategories,
            categoryID: categoryID,
            listData: listData,
            isLoading: false,
        })
    }

    componentDidMount = async () => {
        await this.loadData()
        this.focusListener = this.props.navigation.addListener('didFocus', async () => {
            await this.loadData()
        })
    }

    displayPopular = async () => {
        this.api = new MainAPI()
        const listData = await this.api.getPopularProducts(this.state.categoryID)
        this.setState({
            displayPopular: true,
            displayNew: false,
            displayPrice: false,
            listData: listData
        })
    }

    displayNew = async () => {
        this.api = new MainAPI()
        const listData = await this.api.getNewProducts(this.state.categoryID)
        this.setState({
            displayPopular: false,
            displayNew: true,
            displayPrice: false,
            listData: listData
        })
    }

    displayPrice = async () => {
        this.api = new MainAPI()
        const listData = await this.api.getPriceProducts(this.state.categoryID)
        this.setState({
            displayPopular: false,
            displayNew: false,
            displayPrice: true,
            listData: listData
        })
    }

    goToDetail = (id) => {
        this.props.navigation.navigate('DetailScreen', { id: id })
    }

    getCategories = async () => {
        this.api = new MainAPI()
        return await this.api.getCategories()
    }

    filterByCategories = async (id) => {
        this.setState({
            categoryID: id
        })
        const { displayPopular, displayNew, displayPrice } = this.state
        this.api = new MainAPI()
        let listData = []
        if (displayPopular && !displayNew && !displayPrice) {
            listData = await this.api.getPopularProducts(id)
        }
        else if (!displayPopular && displayNew && !displayPrice) {
            listData = await this.api.getNewProducts(id)
        }
        else if (!displayPopular && !displayNew && displayPrice) {
            listData = await this.api.getPriceProducts(id)
        }
        this.setState({
            listData: listData
        })
    }

    goToConfirmOrder = async () => {
        const token = await AsyncStorage.getItem('Token')
        if (!token) {
            alert('Please login to see your cart')
        } else {
            this.props.navigation.navigate('ConfirmOrder')
        }
    }

    setSearchKey = text => this.setState({ searchKey: text })

    doSearch = async () => {
        this.api = new MainAPI()
        const resText = await this.api.search(this.state.searchKey)
        this.props.navigation.navigate('SearchResult', { data: resText })
    }

    render = () => {
        const { displayPopular, displayNew, displayPrice, listCategories, listData, isLoading, images } = this.state
        if (isLoading) {
            return (
                <ActivityIndicator />
            )
        }
        else {
            return (
                <ScrollView showsVerticalScrollIndicator={false} style={styles.viewAll}>
                    {/* First Block */}
                    <View style={styles.firstBlock}>
                        <TextInput
                            style={styles.searchInput}
                            placeholder='Search For Food...'
                            value={this.state.searchKey}
                            onChangeText={this.setSearchKey}
                            onEndEditing={this.doSearch}
                        />
                        <TouchableOpacity onPress={this.goToConfirmOrder}>
                            <AntDesign name='shoppingcart' size={calcScale(30)} />
                        </TouchableOpacity>
                    </View>

                    {/* Banner */}
                    <SliderBox
                        images={images}
                        sliderBoxHeight={calcScale(150)}
                        autoplay
                        circleLoop
                    />

                    {/* Second Block */}
                    <View style={styles.overAll2ndBlock}>
                        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                            {
                                listCategories.map((item) => {
                                    return (
                                        <TouchableOpacity style={styles.itemCategory} onPress={async () => await this.filterByCategories(item.id)}>
                                            <Image style={{ alignSelf: 'center' }} source={{ uri: item.image_url }} width={calcScale(50)} />
                                            <Text style={{ textAlign: 'center' }}>{item.name}</Text>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </ScrollView>
                    </View>

                    <View style={styles.navigationComponent}>
                        {
                            displayPopular ?
                                <TouchableOpacity onPress={this.displayPopular} style={[styles.itemNavigation, styles.itemNavigationFocus]}>
                                    <Text style={styles.textNavigationFocus}>POPULAR</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={this.displayPopular} style={[styles.itemNavigation]}>
                                    <Text>POPULAR</Text>
                                </TouchableOpacity>
                        }

                        {
                            displayNew ?
                                <TouchableOpacity onPress={this.displayNew} style={[styles.itemNavigation, styles.itemNavigationFocus]}>
                                    <Text style={styles.textNavigationFocus}>NEW</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={this.displayNew} style={styles.itemNavigation}>
                                    <Text>NEW</Text>
                                </TouchableOpacity>
                        }

                        {
                            displayPrice ?
                                <TouchableOpacity onPress={this.displayPrice} style={[styles.itemNavigation, styles.itemNavigationFocus]}>
                                    <Text style={styles.textNavigationFocus}>PRICE</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={this.displayPrice} style={styles.itemNavigation}>
                                    <Text>PRICE</Text>
                                </TouchableOpacity>
                        }

                    </View>

                    <View style={{ marginTop: calcScale(20) }}>
                        {
                            listData.map((item) => {
                                return (
                                    <TouchableOpacity style={styles.listItem} onPress={() => this.goToDetail(item.id)}>
                                        <Image source={{ uri: item.image_url }} width={calcScale(70)} />
                                        <View style={styles.blockNameRatingPrice}>
                                            <Text style={styles.textName}>
                                                {item.name}
                                            </Text>
                                            <View style={styles.blockRatingAndPrice}>
                                                <View style={styles.blockRating}>
                                                    <AntDesign size={calcScale(20)} name='star' color='#f5a623' />
                                                    <Text style={styles.textRating}>{item.rating}</Text>
                                                </View>
                                                <View>
                                                    <Text style={styles.textPrice}>{currencyFormat(Number.parseFloat(item.price))}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
                </ScrollView>
            )
        }
    }
}
export default HomeScreen

const styles = StyleSheet.create({
    viewAll: {
        height: '100%',
        backgroundColor: mainBgr
    },
    firstBlock: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: calcScale(15),
        alignItems: 'center'
    },
    searchInput: {
        width: '80%',
        backgroundColor: '#ececec',
        height: calcScale(50),
        borderRadius: calcScale(20),
        paddingLeft: calcScale(15)
    },
    locationBlock: {
        height: calcScale(45)
    },
    overAll2ndBlock: {
        marginTop: calcScale(10),
        height: calcScale(90)
    },
    itemCategory: {
        padding: calcScale(5),
        borderColor: '#707070',
        borderWidth: calcScale(1),
        borderRadius: calcScale(20),
        marginLeft: calcScale(27),
        width: calcScale(100)
    },
    navigationComponent: {
        flexDirection: 'row',
        marginTop: calcScale(10),
    },
    itemNavigation: {
        width: '33%',
        alignItems: 'center',
        borderBottomWidth: calcScale(1),
        borderBottomColor: '#707070',
        paddingBottom: calcScale(5),
    },
    textNavigationFocus: {
        color: '#147fad',
        fontWeight: 'bold'
    },
    itemNavigationFocus: {
        borderBottomColor: '#147fad',
        borderBottomWidth: calcScale(2)
    },
    listItem: {
        flexDirection: 'row',
        paddingHorizontal: calcScale(15),
        marginBottom: calcScale(15)
    },
    blockNameRatingPrice: {
        marginLeft: calcScale(10),
        flex: 1
    },
    blockRatingAndPrice: {
        marginTop: calcScale(20),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    blockRating: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textPrice: {
        color: '#147fad',
        fontStyle: 'italic',
        fontSize: calcScale(20)
    },
    textName: {
        fontSize: calcScale(20)
    },
    textRating: {
        marginLeft: calcScale(5),
        fontSize: calcScale(20)
    }
})