import * as React from 'react'
import { StyleSheet, View, TouchableOpacity, ImageBackground, Text, ActivityIndicator, ToastAndroid } from 'react-native'
import { calcScale, mainBgr, currencyFormat } from '../constants/Constants'
import HeaderComponent from '../component/HeaderComponent'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import AntDesign from 'react-native-vector-icons/AntDesign'
import MainAPI from '../api/MainAPI'
import AsyncStorage from '@react-native-community/async-storage';

export default class DetailScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            amount: 1,
            data: {},
            isLoading: true
        }
    }

    componentDidMount = async () => {
        this.api = new MainAPI()
        const data = await this.api.getDetailProducts(this.props.navigation.getParam('id'))
        this.setState({
            data: data,
            isLoading: false
        })
    }

    upAmount = () => {
        this.setState({
            amount: this.state.amount + 1
        })
    }

    downAmount = () => {
        this.setState({
            amount: this.state.amount - 1
        })
        if (this.state.amount === 1) {
            this.setState({
                amount: 1
            })
        }
    }

    confirmOrder = async (data) => {
        const token = await AsyncStorage.getItem('Token')
        if (!token) {
            alert('Please login to confirm your order')
        }
        else {
            this.api = new MainAPI()
            const resText = await this.api.addToCart(data.id, this.state.amount)
            if (resText.status = 200) {
                ToastAndroid.show('Your item has been added to cart', ToastAndroid.LONG)
            }
        }
    }

    back = () => {
        this.props.navigation.navigate('HomeScreen')
    }

    render = () => {
        const { data, isLoading } = this.state
        if (isLoading) {
            return (
                <ActivityIndicator />
            )
        }
        else {
            return (
                <View style={styles.viewAll}>
                    <ImageBackground source={{ uri: data.image_url }} style={{ width: '100%', height: calcScale(150), opacity: 1 }}>
                        <HeaderComponent
                            style={{ padding: calcScale(20) }}
                            buttonRightStyle={{ opacity: 0 }}
                            name='left'
                            backOnPress={this.back}
                        />
                    </ImageBackground>

                    <View style={{ marginTop: calcScale(20), paddingHorizontal: calcScale(20) }}>
                        {/* Name and description */}
                        <Text style={styles.textName}>{data.name}</Text>
                        <Text style={styles.textDescription}>
                            {data.description}
                        </Text>

                        {/* Rating and favo */}
                        <View style={styles.ratingAndFavorite}>
                            <View style={styles.ratingComp}>
                                <View style={styles.iconRating}>
                                    <AntDesign color='#fff' size={calcScale(50)} name='staro' />
                                </View>
                                <View style={{ marginLeft: calcScale(10), alignSelf: 'center' }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: calcScale(25) }}>{data.rating}</Text>
                                    <Text style={{ fontSize: calcScale(20), color: '#707070' }}>Rating</Text>
                                </View>
                            </View>

                            <View style={styles.favoriteComp}>
                                <View style={styles.iconFavorite}>
                                    <AntDesign color='#fff' size={calcScale(45)} name='hearto' />
                                </View>
                                <View style={{ marginLeft: calcScale(10), alignSelf: 'center' }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: calcScale(25) }}>200</Text>
                                    <Text style={{ fontSize: calcScale(20), color: '#707070' }}>Favorite</Text>
                                </View>
                            </View>
                        </View>

                        {/* Amount select */}
                        <View style={styles.amountSelectComp}>
                            <TouchableOpacity style={styles.buttonUpDownAmount} onPress={this.downAmount}>
                                <Text style={styles.textUpDownAmount}>-</Text>
                            </TouchableOpacity>
                            <Text style={styles.textAmount}>{this.state.amount}</Text>
                            <TouchableOpacity style={styles.buttonUpDownAmount} onPress={this.upAmount}>
                                <Text style={styles.textUpDownAmount}>+</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Submit Button */}
                        <TouchableOpacity style={styles.submitButton} onPress={() => this.confirmOrder(data)}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: calcScale(20) }}>Order</Text>
                                <Text style={{ fontSize: calcScale(20) }}>{currencyFormat(Number.parseFloat(data.price) * this.state.amount)}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    viewAll: {
        height: '100%',
        backgroundColor: mainBgr
    },
    heartComp: {
        padding: calcScale(10),
        borderRadius: calcScale(15),
        backgroundColor: '#f3c4ec'
    },
    textName: {
        fontWeight: 'bold',
        fontSize: calcScale(25),
        textAlign: 'center'
    },
    textDescription: {
        fontSize: calcScale(20),
        color: '#707070'
    },
    ratingAndFavorite: {
        flexDirection: 'row',
        marginTop: calcScale(20),
        alignSelf: 'center'
    },
    ratingComp: {
        flexDirection: 'row'
    },
    iconRating: {
        backgroundColor: '#fcaf17',
        width: calcScale(70),
        height: calcScale(70),
        borderRadius: calcScale(35),
        alignItems: 'center',
        justifyContent: 'center'
    },
    favoriteComp: {
        flexDirection: 'row',
        marginLeft: calcScale(40)
    },
    iconFavorite: {
        backgroundColor: '#da4453',
        width: calcScale(70),
        height: calcScale(70),
        borderRadius: calcScale(35),
        alignItems: 'center',
        justifyContent: 'center'
    },
    amountSelectComp: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: calcScale(20),
        alignSelf: 'center'
    },
    buttonUpDownAmount: {
        paddingHorizontal: calcScale(30),
        paddingVertical: calcScale(20),
        borderColor: '#707070',
        borderWidth: calcScale(1),
        borderRadius: calcScale(15)
    },
    textUpDownAmount: {
        fontSize: calcScale(30),
        fontWeight: 'bold'
    },
    textAmount: {
        fontSize: calcScale(30),
        fontWeight: 'bold',
        marginHorizontal: calcScale(20)
    },
    submitButton: {
        padding: calcScale(20),
        backgroundColor: '#f3e64e',
        marginTop: calcScale(20),
        borderRadius: calcScale(30),
        width: '70%',
        alignSelf: 'center'
    }
})