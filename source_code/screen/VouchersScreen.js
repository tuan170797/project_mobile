import * as React from 'react'
import { View, ScrollView, Text, StyleSheet, } from 'react-native'
import { calcScale, mainBgr } from './../constants/Constants'
import HeaderComponent from './../component/HeaderComponent'
import Image from 'react-native-scalable-image'
import MainAPI from './../api/MainAPI'

export default class VouchersScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            listVouchers: []
        }
    }

    componentDidMount = async () => {
        this.api = new MainAPI()
        const listVouchers = await this.api.getListVouchers()
        this.setState({
            listVouchers: listVouchers.data
        })
    }

    render = () => {
        const { listVouchers } = this.state
        console.log(listVouchers)
        return (
            <ScrollView style={styles.viewAll}>
                <HeaderComponent
                    title='Voucher Wallet'
                    backOnPress={() => this.props.navigation.goBack()}
                    style={styles.header}
                    nameButtonRight='left'
                    buttonRightStyle={{opacity: 0}}
                />
                {
                    listVouchers.map(item => {
                        return (
                            <View style={styles.itemComp}>
                                <Image source={require('./../image/voucher.jpg')} width={calcScale(70)} />
                                <View style={{ marginLeft: calcScale(10), flex: 1 }}>
                                    <View style={styles.itemTopBottomComp}>
                                        <Text style={{ fontSize: calcScale(20) }}>{item.voucher_code}</Text>
                                    </View>
                                    <View style={[styles.itemTopBottomComp, { marginTop: calcScale(10) }]}>
                                        <View style={styles.blockRating}>
                                            <Text style={styles.textRating}>Discount {Number.parseFloat(item.discount) * 100}%</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        )
                    })
                }
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    viewAll: {
        height: '100%',
        backgroundColor: mainBgr,
        paddingHorizontal: calcScale(20)
    },
    header: {
        marginTop: calcScale(30),
        marginBottom: calcScale(20)
    },
    itemComp: {
        flexDirection: 'row',
        marginTop: calcScale(10),
        backgroundColor: '#fff',
        padding: calcScale(15),
    },
    itemTopBottomComp: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    blockRating: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textPrice: {
        color: '#e99098',
        fontStyle: 'italic',
        fontSize: calcScale(15)
    },
    textRating: {
        color: '#707070',
        fontSize: calcScale(15)
    }
})