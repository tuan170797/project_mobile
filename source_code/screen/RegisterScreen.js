import * as React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, TextInput, Alert, ActivityIndicator, Image } from 'react-native';
import { calcScale, mainBgr } from './../constants/Constants'
import HeaderComponent from './../component/HeaderComponent'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Modal from 'react-native-modal'
import MainAPI from './../api/MainAPI'

class RegisterScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            fullName: '',
            password: '',
            repeatPsw: '',
            email: '',
            address: '',
            phone: '',
            code: '',
            modalIsOpen: false,
            isLoading: true,
        }
    }

    componentDidMount = async () => {
        if (this.props.navigation.getParam('title') === 'Edit Profile') {
            this.api = new MainAPI()
            const userInfo = await this.api.getUserInfo()
            this.setState({
                username: userInfo.username,
                fullName: userInfo.fullname,
                email: userInfo.email,
                phone: userInfo.phone,
                address: userInfo.address,
                isLoading: false
            })
        }
        else {
            this.setState({ isLoading: false })
        }
    }

    setUsername = text => this.setState({ username: text })

    setFullName = text => this.setState({ fullName: text })

    setPassword = text => this.setState({ password: text })

    setRepeatPsw = text => this.setState({ repeatPsw: text })

    setEmail = text => this.setState({ email: text })

    setPhone = text => this.setState({ phone: text })

    setAddress = text => this.setState({ address: text })

    setCode = text => this.setState({ code: text })

    submit = async () => {
        const { username, fullName, password, email, repeatPsw, phone } = this.state

        let obj = { username, fullName, password, email, phone }

        console.log(obj)

        if (this.props.navigation.getParam('title') === 'Register Account') {
            if (password !== repeatPsw) {
                Alert.alert('Oops', 'Repeat password is not match with password')
            }
            else {
                this.setState({ isLoading: true })
                this.api = new MainAPI()
                const resText = await this.api.register(obj)
                console.log(resText)
                if (resText.status === 'confirm') {
                    this.setState({
                        isLoading: false,
                        modalIsOpen: true
                    })
                }
                else if (resText.status === 'require') {
                    this.setState({
                        isLoading: false
                    })
                    Alert.alert('Oops', 'Some fields are required T.T')
                }
                else {
                    let mess = ''
                    this.setState({
                        isLoading: false
                    })
                    const errMess = resText.message
                    if (errMess.email) {
                        mess += errMess.email + '\n'
                    }
                    if (errMess.password) {
                        mess += errMess.password + '\n'
                    }
                    if (errMess.username) {
                        mess += errMess.username + '\n'
                    }
                    if (errMess.phone) {
                        mess += errMess.phone[0] + '\n'
                    }
                    Alert.alert('Oops', mess)
                }
            }
        }
        else if (this.props.navigation.getParam('title') === 'Edit Profile') {
            this.setState({ isLoading: true })
            this.api = new MainAPI()
            const resText = await this.api.submitEditInfo(obj)
            if (resText.status === 'success') {
                this.setState({
                    isLoading: false
                })
                this.props.navigation.navigate('UserScreen')
            }
            else {
                this.setState({
                    isLoading: false
                })
                Alert.alert('Oops', 'Missing require fullname or phone')
            }
        }

    }

    submitCode = async () => {
        this.setState({
            modalIsOpen: false
        })
        const { username, code } = this.state
        this.api = new MainAPI()
        const resText = await this.api.confirmCodeRegister(username, code)
        if (resText.status === 'success') {
            this.props.navigation.navigate('LoginScreen')
            Alert.alert('Congratulation', 'Your account has been created successfully')
        }
    }

    renderModalConfirmCode = (modalIsOpen = false) => {
        return (
            <Modal isVisible={modalIsOpen}>
                <View style={{ backgroundColor: '#fff', padding: calcScale(20) }}>
                    <HeaderComponent
                        title='Verify your account'
                        titleStyle={{ color: '#f6ad34' }}
                        backOnPress={() => this.setState({ modalIsOpen: false })}
                        onPressButtonRight={this.submitCode}
                        nameButtonRight='check'
                    />
                    <Text style={{ marginTop: calcScale(20), color: 'red' }}>
                        Please enter verification code we have sent via email
                    </Text>

                    <TextInput
                        placeholder='Enter code'
                        style={[styles.inputField, { marginTop: calcScale(20) }]}
                        value={this.state.code}
                        onChangeText={this.setCode}
                    />
                </View>
            </Modal>
        )
    }

    render = () => {
        let editable = true
        if (this.props.navigation.getParam('title') === 'Edit Profile') {
            editable = false
        }
        if (this.state.isLoading) {
            return (
                <ActivityIndicator />
            )
        }
        else {
            return (
                <ScrollView style={styles.viewAll} showsVerticalScrollIndicator={false}>
                    {/* Header */}
                    <HeaderComponent
                        title={this.props.navigation.getParam('title')}
                        style={{ paddingHorizontal: calcScale(20) }}
                        backOnPress={() => this.props.navigation.navigate('UserScreen')}
                        buttonRightStyle={{ opacity: 0 }}
                        nameButtonRight='left'
                    />
                    {/* Top Component */}
                    <View style={styles.topComponent}>
                        {
                            this.props.navigation.getParam('title') === 'Edit Profile' ?
                                <Image style={{width: calcScale(150), height: calcScale(150), borderRadius: calcScale(75)}} source={require('./../image/idol2.jpg')} />
                                : <Icon size={calcScale(150)} name='user-circle' />
                        }

                        <TextInput
                            placeholder='Enter your username'
                            style={styles.inputName}
                            value={this.state.username}
                            onChangeText={this.setUsername}
                            editable={editable}
                        />
                    </View>
                    {this.renderModalConfirmCode(this.state.modalIsOpen)}
                    {/* Bottom Component */}
                    <View style={styles.bottomComponent}>
                        <View style={styles.field}>
                            <Text style={styles.textOfField}>Full Name</Text>
                            <TextInput style={styles.inputField} value={this.state.fullName} onChangeText={this.setFullName} />
                        </View>
                        {
                            this.props.navigation.getParam('title') !== 'Edit Profile' ?
                                (<View>
                                    <View style={styles.field}>
                                        <Text style={styles.textOfField}>Password</Text>
                                        <TextInput secureTextEntry={true} style={styles.inputField} value={this.state.password} onChangeText={this.setPassword} />
                                    </View>

                                    <View style={styles.field}>
                                        <Text style={styles.textOfField}>Re-enter Password</Text>
                                        <TextInput secureTextEntry={true} style={styles.inputField} value={this.state.repeatPsw} onChangeText={this.setRepeatPsw} />
                                    </View>
                                </View>) : null
                        }

                        <View style={styles.field}>
                            <Text style={styles.textOfField}>Email</Text>
                            <TextInput style={styles.inputField} value={this.state.email} onChangeText={this.setEmail} />
                        </View>

                        <View style={styles.field}>
                            <Text style={styles.textOfField}>Phone</Text>
                            <TextInput style={styles.inputField} value={this.state.phone} onChangeText={this.setPhone} />
                        </View>

                        <View style={styles.field}>
                            <Text style={styles.textOfField}>Address</Text>
                            <TextInput style={styles.inputField} value={this.state.address} onChangeText={this.setAddress} />
                        </View>

                        <TouchableOpacity style={styles.buttonSubmit} onPress={this.submit}>
                            <Text style={{ textAlign: 'center' }}>Save</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            )
        }
    }
}

export default RegisterScreen

const styles = StyleSheet.create({
    viewAll: {
        height: '100%',
        backgroundColor: mainBgr,
        paddingVertical: calcScale(20)
    },
    topComponent: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: calcScale(20),
        marginTop: calcScale(20)
    },
    inputName: {
        borderBottomColor: 'green',
        borderBottomWidth: calcScale(2),
        width: '80%'
    },
    bottomComponent: {
        backgroundColor: '#fff',
        paddingTop: calcScale(20),
        paddingBottom: calcScale(50),
        marginTop: calcScale(15),
        paddingHorizontal: calcScale(30),
    },
    field: {
        marginTop: calcScale(10),
    },
    textOfField: {
        color: '#707070',
        marginLeft: calcScale(10),
        marginBottom: calcScale(5)
    },
    inputField: {
        width: '100%',
        borderColor: '#1a1a1a',
        borderWidth: calcScale(1),
        borderRadius: calcScale(5),
        paddingHorizontal: calcScale(15)
    },
    buttonSubmit: {
        padding: calcScale(10),
        backgroundColor: '#f7ba56',
        marginTop: calcScale(20),
        width: '30%',
        borderRadius: calcScale(15),
        alignSelf: 'flex-end'
    }
})