import * as React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Alert, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { calcScale, mainBgr } from '../constants/Constants'
import HeaderComponent from '../component/HeaderComponent'
import Session from './../constants/Session'
import AsyncStorage from '@react-native-community/async-storage';
import Modal from 'react-native-modal'
import MainAPI from './../api/MainAPI'

class UserScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            isLoggedIn: '',
            psw: '',
            newPsw: '',
            repeatNewPsw: '',
            modalIsOpen: false
        }
        this.configFields = [
            {
                iconName: 'tag',
                name: 'Vouchers',
                onPressFunction: async () => {
                    const token = await AsyncStorage.getItem('Token')
                    if (!token) {
                        alert('Please login to see your vouchers')
                    } else {
                        this.props.navigation.navigate('VouchersScreen')
                    }
                }
            },
            { iconName: 'wallet', name: 'Payment', onPressFunction: this.notSupport },
            { iconName: 'contacts', name: 'Address', onPressFunction: this.notSupport },
        ]
        this.token = null
        this.username = null
    }

    notSupport = () => Alert.alert('!!!', 'Coming soon...')

    componentDidMount = async () => {
        this.token = await AsyncStorage.getItem('Token')
        this.username = await AsyncStorage.getItem('Username')
        if (this.token && this.username) {
            this.setState({
                username: this.username,
                isLoggedIn: true,
            })
        }
        this.focusListener = this.props.navigation.addListener('didFocus', async () => {
            this.token = await AsyncStorage.getItem('Token')
            this.username = await AsyncStorage.getItem('Username')
            if (this.token && this.username) {
                this.setState({
                    username: this.username,
                    isLoggedIn: true,
                })
            }
        })
    }

    openPopUpChangePassword = () => {
        this.setState({ modalIsOpen: true })
    }

    confirmChangePassword = async () => {
        const { psw, newPsw, repeatNewPsw } = this.state

        const obj = { psw, newPsw }

        if (!newPsw && !psw) {
            Alert.alert('Oops', 'Please input all of information')
        }
        else if (newPsw !== repeatNewPsw) {
            Alert.alert('Oops', 'Repeat new password does not match with new password')
        }
        else {
            this.api = new MainAPI()
            const resText = await this.api.confirmChangePsw(obj)
            if (resText.status === 'success') {
                this.setState({
                    modalIsOpen: false
                })
                Alert.alert('Congratulation', 'Your password has been changed')
            }
            else {
                Alert.alert('Oops', resText.message)
            }
        }
    }

    setPsw = text => this.setState({ psw: text })

    setNewPsw = text => this.setState({ newPsw: text })

    setRepeatNewPsw = text => this.setState({ repeatNewPsw: text })

    popUpChangePassword = (modalIsOpen) => {
        return (
            <Modal isVisible={modalIsOpen}>
                <View style={{ backgroundColor: '#fff', padding: calcScale(20) }}>
                    <HeaderComponent
                        title='Change Password'
                        titleStyle={{ color: '#f6ad34' }}
                        backOnPress={() => this.setState({ modalIsOpen: false })}
                        nameButtonRight='check'
                        onPressButtonRight={this.confirmChangePassword}
                    />
                    <TextInput
                        placeholder='Old Password'
                        style={[styles.inputField, { marginTop: calcScale(20) }]}
                        value={this.state.psw}
                        onChangeText={this.setPsw}
                        secureTextEntry={true}
                    />

                    <TextInput
                        placeholder='New Password'
                        style={[styles.inputField, { marginTop: calcScale(20) }]}
                        value={this.state.newPsw}
                        onChangeText={this.setNewPsw}
                        secureTextEntry={true}
                    />

                    <TextInput
                        placeholder='Repeat New Password'
                        style={[styles.inputField, { marginTop: calcScale(20) }]}
                        value={this.state.repeatNewPsw}
                        onChangeText={this.setRepeatNewPsw}
                        secureTextEntry={true}
                    />
                </View>
            </Modal>
        )
    }

    doSignOut = async () => {
        this.session = new Session()
        await this.session.logout()
        this.setState({
            username: '',
            isLoggedIn: false,
        })
    }

    renderFunction = (
        iconName = '',
        name = '',
        onPressFunction = () => { }
    ) => {
        return (
            <TouchableOpacity style={styles.wrapperFunction} onPress={onPressFunction}>
                <AntDesign color='#707070' size={calcScale(30)} name={iconName} />
                <Text style={{ fontSize: calcScale(16), marginLeft: calcScale(20), color: '#707070' }}>{name}</Text>
            </TouchableOpacity>
        )
    }

    renderFields = () => {
        return this.configFields.map(item => {
            return this.renderFunction(item.iconName, item.name, item.onPressFunction)
        })
    }

    goToLoginScreen = () => this.props.navigation.navigate('LoginScreen')

    goToRegisterScreen = () => this.props.navigation.navigate('RegisterScreen', { title: 'Register Account' })

    goToEditScreen = () => this.props.navigation.navigate('RegisterScreen', { title: 'Edit Profile' })

    render = () => {
        return (
            <View style={styles.viewAll}>
                <View style={styles.topComponent}>
                    {
                        this.state.isLoggedIn ?
                            <Image style={{width: calcScale(150), height: calcScale(150), borderRadius: calcScale(75)}} source={require('./../image/idol2.jpg')}/>
                            :
                            <Icon size={calcScale(150)} name='user-circle' />
                    }

                    {
                        this.state.isLoggedIn ?
                            <View style={styles.functionComponent}>
                                <Text style={{ fontSize: calcScale(18), color: '#24b2ad' }}>{this.state.username}</Text>
                            </View>
                            :
                            <View style={styles.functionComponent}>
                                <TouchableOpacity style={styles.functionButton} onPress={this.goToLoginScreen}>
                                    <Text style={{ color: '#fff' }}>Login</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.functionButton, { marginLeft: calcScale(10) }]} onPress={this.goToRegisterScreen}>
                                    <Text style={{ color: '#fff' }}>Register</Text>
                                </TouchableOpacity>
                            </View>
                    }

                </View>

                {this.popUpChangePassword(this.state.modalIsOpen)}

                <View style={styles.bottomComponent}>
                    {this.renderFields()}
                </View>

                {
                    this.state.isLoggedIn ?
                        <TouchableOpacity style={styles.buttonSignOut} onPress={this.goToEditScreen}>
                            <Text style={styles.textSignOut}>Edit Account</Text>
                        </TouchableOpacity>
                        :
                        null
                }

                {
                    this.state.isLoggedIn ?
                        <TouchableOpacity style={styles.buttonSignOut} onPress={this.openPopUpChangePassword}>
                            <Text style={styles.textSignOut}>Change Password</Text>
                        </TouchableOpacity>
                        :
                        null
                }

                {
                    this.state.isLoggedIn ?
                        <TouchableOpacity style={styles.buttonSignOut} onPress={this.doSignOut}>
                            <Text style={styles.textSignOut}>Sign Out</Text>
                        </TouchableOpacity>
                        :
                        null
                }


            </View>
        )
    }
}
export default UserScreen

const styles = StyleSheet.create({
    viewAll: {
        backgroundColor: mainBgr,
        height: '100%',
    },
    topComponent: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: calcScale(20)
    },
    functionComponent: {
        flexDirection: 'row',
        marginTop: calcScale(10)
    },
    functionButton: {
        padding: calcScale(10),
        backgroundColor: '#f7ba56',
        borderRadius: calcScale(5),
        width: calcScale(100),
        alignItems: 'center'
    },
    wrapperFunction: {
        flexDirection: 'row',
        borderBottomWidth: calcScale(1),
        borderBottomColor: '#707070',
        paddingVertical: calcScale(15)
    },
    bottomComponent: {
        paddingHorizontal: calcScale(30),
        marginTop: calcScale(30)
    },
    textEdit: {
        color: '#24b2ad',
        fontWeight: 'bold',
        fontSize: calcScale(20)
    },
    buttonSignOut: {
        backgroundColor: '#f7ba56',
        padding: calcScale(15),
        borderRadius: calcScale(20),
        marginTop: calcScale(10),
        marginHorizontal: calcScale(30)
    },
    textSignOut: {
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#fff'
    },
    inputField: {
        width: '100%',
        borderColor: '#1a1a1a',
        borderWidth: calcScale(1),
        borderRadius: calcScale(5),
        paddingHorizontal: calcScale(15)
    },
})