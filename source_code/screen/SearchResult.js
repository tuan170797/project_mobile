import * as React from 'react'
import { View, ScrollView, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { calcScale, mainBgr, currencyFormat } from './../constants/Constants'
import Image from 'react-native-scalable-image'
import AntDesign from 'react-native-vector-icons/AntDesign'
import HeaderComponent from './../component/HeaderComponent'
export default class FavoriteScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }

    componentDidMount = async () => {
        const data = await this.props.navigation.getParam('data')
        this.setState({
            data: data
        })
    }

    goToDetail = id => this.props.navigation.navigate('DetailScreen', { id: id })

    render = () => {
        const { data } = this.state
        return (
            <ScrollView style={styles.viewAll}>
                {/* Header */}
                <HeaderComponent
                    title='Search Result'
                    style={styles.header}
                    backOnPress={() => this.props.navigation.navigate('HomeScreen')}
                    nameButtonRight='left'
                    buttonRightStyle={{opacity: 0}}
                />

                {/* List Search */}
                {
                    data.map(item => {
                        return (
                            <TouchableOpacity style={styles.itemComp} onPress={() => this.goToDetail(item.id)}>
                                <Image source={{ uri: item.image_url }} width={calcScale(70)} height={calcScale(70)} />
                                <View style={{ marginLeft: calcScale(10), flex: 1 }}>
                                    <View style={styles.itemTopBottomComp}>
                                        <Text style={{ fontSize: calcScale(20) }}>{item.name}</Text>
                                    </View>
                                    <View style={[styles.itemTopBottomComp, { marginTop: calcScale(10) }]}>
                                        <View style={styles.blockRating}>
                                            <AntDesign size={calcScale(20)} name='star' color='#f5a623' />
                                            <Text style={styles.textRating}>{item.rating}</Text>
                                        </View>
                                        <View>
                                            <Text style={styles.textPrice}>{currencyFormat(Number.parseFloat(item.price))}</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )
                    })
                }
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    viewAll: {
        backgroundColor: mainBgr,
        height: '100%',
        paddingHorizontal: calcScale(20),
    },
    header: {
        marginTop: calcScale(20),
        marginBottom: calcScale(30)
    },
    itemComp: {
        flexDirection: 'row',
        marginTop: calcScale(10)
    },
    itemTopBottomComp: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    blockRating: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textPrice: {
        color: '#147fad',
        fontStyle: 'italic',
        fontSize: calcScale(20)
    },
    textRating: {
        marginLeft: calcScale(5),
        fontSize: calcScale(20)
    }
})