import * as React from 'react'
import { View, ScrollView, Text, StyleSheet, TextInput, TouchableOpacity, ActivityIndicator, Alert, Image } from 'react-native'
import { calcScale, mainBgr, currencyFormat } from './../constants/Constants'
import HeaderComponent from './../component/HeaderComponent'
import AntDesign from 'react-native-vector-icons/AntDesign'
import ImageScale from 'react-native-scalable-image'
import DateTimePicker from 'react-native-modal-datetime-picker'
import Modal from 'react-native-modal'
import TimePicker from 'react-native-24h-timepicker'
import moment from 'moment'
import MainAPI from './../api/MainAPI'

export default class ConfirmOrder extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            startTime: '',
            endTime: '',
            date: '',
            visibleDatePicker: false,
            modalIsOpen: false,
            fullTime: '',
            listOrder: [],
            sumPrice: 0,
            voucherCode: '',
            userInfo: {},
            discount: 0,
            isLoading: true
        }
    }

    componentDidMount = async () => {
        this.api = new MainAPI()
        const listOrder = await this.api.getListCartByUser()
        const userInfo = await this.api.getUserInfo()
        let sumPrice = 0
        if (listOrder) {
            listOrder.data.forEach(item => {
                sumPrice += Number.parseFloat(item.price) * item.amount
            })
            this.setState({
                listOrder: listOrder.data,
                sumPrice: sumPrice,
                userInfo: userInfo,
                isLoading: false
            })
        }
        else {
            this.setState({
                listOrder: [],
                userInfo: userInfo,
                isLoading: false
            })
        }
    }

    onConfirmStart = (hour, minute) => {
        this.setState({
            startTime: `${hour}:${minute}`
        })
        this.TimePickerStart.close()
    }

    onConfirmEnd = (hour, minute) => {
        this.setState({
            endTime: `${hour}:${minute}`
        })
        this.TimePickerEnd.close()
    }

    showDatePicker = () => {
        this.setState({
            visibleDatePicker: true
        })
    }

    hideDatePicker = () => {
        this.setState({
            visibleDatePicker: false
        })
    }

    onConfirmDate = (datetime) => {
        this.setState({
            date: moment(datetime).format('YYYY-MM-DD'),
            visibleDatePicker: false
        })
    }

    submitTime = () => {
        this.setState({
            modalIsOpen: false,
            fullTime: `${this.state.startTime} - ${this.state.endTime} - ${moment(this.state.date).format('MMM Do, YYYY')}`
        })
    }

    renderModal = modalIsOpen => {
        return (
            <Modal isVisible={modalIsOpen}>
                <View style={{ backgroundColor: '#fff', paddingVertical: calcScale(20) }}>
                    <HeaderComponent
                        title='Select Time'
                        onPressButtonRight={this.submitTime}
                        nameButtonRight='check'
                        style={{ paddingHorizontal: calcScale(20) }}
                        backOnPress={() => this.setState({ modalIsOpen: false })}
                    />
                    <View style={{ paddingHorizontal: calcScale(20) }}>
                        <View style={[styles.flexRow, { alignItems: 'center' }]}>
                            <Text style={{ flex: 3 }}>Start Time</Text>
                            <TouchableOpacity style={styles.fieldModal} onPress={() => this.TimePickerStart.open()}>
                                <Text>{this.state.startTime}</Text>
                            </TouchableOpacity>
                            <TimePicker
                                ref={ref => {
                                    this.TimePickerStart = ref
                                }}
                                onCancel={() => this.TimePickerStart.close()}
                                onConfirm={(hour, minute) => this.onConfirmStart(hour, minute)}
                            />
                        </View>

                        <View style={[styles.flexRow, { alignItems: 'center' }]}>
                            <Text style={{ flex: 3 }}>End Time</Text>
                            <TouchableOpacity style={styles.fieldModal} onPress={() => this.TimePickerEnd.open()}>
                                <Text>{this.state.endTime}</Text>
                            </TouchableOpacity>
                            <TimePicker
                                ref={ref => {
                                    this.TimePickerEnd = ref
                                }}
                                onCancel={() => this.TimePickerEnd.close()}
                                onConfirm={(hour, minute) => this.onConfirmEnd(hour, minute)}
                            />
                        </View>

                        <View style={[styles.flexRow, { alignItems: 'center' }]}>
                            <Text style={{ flex: 3 }}>Date</Text>
                            <TouchableOpacity style={styles.fieldModal} onPress={this.showDatePicker}>
                                <Text>{this.state.date}</Text>
                            </TouchableOpacity>
                            <DateTimePicker
                                isVisible={this.state.visibleDatePicker}
                                onCancel={this.hideDatePicker}
                                onConfirm={this.onConfirmDate}
                                mode={'date'}
                                minimumDate={moment().add(1, 'day').toDate()}

                            />
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    back = () => this.props.navigation.navigate('HomeScreen')

    removeCart = async (cart_id) => {
        this.api = new MainAPI()
        const resText = await this.api.removeCart(cart_id)
        if (resText.status === 200) {
            this.api = new MainAPI()
            const listOrder = await this.api.getListCartByUser()
            let sumPrice = 0
            if (listOrder) {
                listOrder.data.forEach(item => {
                    sumPrice += Number.parseFloat(item.price) * item.amount
                })
                this.setState({
                    listOrder: listOrder.data,
                    sumPrice: sumPrice,
                })
            }
            else {
                this.setState({
                    listOrder: [],
                })
            }
        }
    }

    submit = async () => {
        const { startTime, endTime, date, listOrder, sumPrice, voucherCode } = this.state

        let cart = []

        listOrder.forEach(item => {
            cart.push(
                {
                    product_id: item.product_id,
                    quantity: item.amount
                }
            )
        })

        let obj = {
            time_from: startTime, time_to: endTime,
            date: date, cart: cart, total_price: sumPrice,
            status: 'Paid', payment_id: 1, voucherCode
        }

        if (cart.length === 0) {
            Alert.alert('Oops', "Your cart doesn't have any item. Please select an item you want")
        }
        if (!this.state.fullTime) {
            Alert.alert('Oops', 'Please select date and time you want')
        }
        else {
            this.setState({
                isLoading: true
            })
            this.api = new MainAPI()
            const resText = await this.api.insertOrder(obj)
            if (resText.status === 200) {
                this.props.navigation.navigate('ReceiptScreen', { data: resText.data, prevScreen: this.props.navigation.state.routeName })
            }
            else if (resText.status === 'error') {
                this.setState({
                    isLoading: false
                })
                Alert.alert('Oops', resText.message)
            }
        }
    }

    setVoucherCode = text => this.setState({ voucherCode: text })

    applyVoucherCode = async () => {
        const { voucherCode } = this.state
        this.api = new MainAPI()
        const resText = await this.api.applyVoucher(voucherCode)
        if (resText.status == 200) {
            const discount = Number.parseFloat(resText.discount)
            this.setState({
                discount: `${discount * 100}%`,
                sumPrice: this.state.sumPrice * (1 - discount)
            })
        }
        else if (resText.status == 400) {
            Alert.alert('Oops', resTExt.message)
        }
    }

    notSupport = () => Alert.alert('Oops', 'Sorry, this payment method is maintained')

    render = () => {
        const { listOrder, isLoading, sumPrice, userInfo, discount } = this.state
        if (isLoading) {
            return (
                <ActivityIndicator />
            )
        }
        else {
            return (
                <ScrollView style={styles.viewAll}>
                    <HeaderComponent
                        title='Confirm Order'
                        buttonRightStyle={{ opacity: 0 }}
                        nameButtonRight='left'
                        style={styles.header}
                        backOnPress={this.back}
                    />
                    {/* Information Component */}
                    <View style={styles.infoComponent}>
                        <Text>Information</Text>
                        <View style={[styles.flexRow, { marginTop: calcScale(10) }]}>
                            <Image style={{ width: calcScale(40), height: calcScale(40), borderRadius: calcScale(20) }} source={require('./../image/idol2.jpg')} />
                            <View style={styles.nameAndAddress}>
                                <Text>{userInfo.fullname} - {userInfo.phone}</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={[styles.flexRowAndSpace, styles.timeComponent]} onPress={() => this.setState({ modalIsOpen: true })}>
                            <View style={styles.flexRow}>
                                <AntDesign size={calcScale(15)} name='clockcircleo' style={{ alignSelf: 'center' }} />
                                <Text style={{ marginLeft: calcScale(5) }}>{this.state.fullTime}</Text>
                            </View>
                            <AntDesign size={calcScale(15)} name='right' />
                            {this.renderModal(this.state.modalIsOpen)}
                        </TouchableOpacity>
                    </View>

                    {/* List Order */}
                    <View style={styles.infoComponent}>
                        {
                            listOrder.map(item => {
                                return (
                                    <View style={styles.flexRowAndSpace}>
                                        <ImageScale style={{ flex: 1 }} width={calcScale(50)} source={{ uri: item.imageUrl }} />
                                        <View style={{ flex: 6, marginLeft: calcScale(10) }}>
                                            <Text>{item.amount} x {item.name}</Text>
                                            <Text style={styles.textPrice}>{currencyFormat(Number.parseFloat(item.price * item.amount))}</Text>
                                        </View>
                                        <TouchableOpacity style={{ flex: 0.5 }} onPress={() => this.removeCart(item.id)}>
                                            <AntDesign name='close' color='red' size={calcScale(20)} />
                                        </TouchableOpacity>
                                    </View>
                                )
                            })
                        }
                    </View>

                    {/* Calculate Amount */}
                    <View style={styles.infoComponent}>
                        {
                            discount ?
                                <View>
                                    <View style={[styles.flexRowAndSpace, { justifyContent: 'space-between' }]}>
                                        <Text style={{ fontWeight: 'bold' }}>Discount</Text>
                                        <Text style={[styles.textPrice, { fontWeight: 'bold' }]}>{discount}</Text>
                                    </View>
                                </View>
                                : null
                        }
                        <View style={[styles.flexRowAndSpace, { justifyContent: 'space-between', marginBottom: 0 }]}>
                            <Text style={{ fontWeight: 'bold' }}>Tổng cộng</Text>
                            <Text style={[styles.textPrice, { fontWeight: 'bold' }]}>{currencyFormat(sumPrice)}</Text>
                        </View>
                    </View>

                    {/* Promotion */}
                    <View style={styles.infoComponent}>
                        <View style={[styles.flexRowAndSpace, { justifyContent: 'space-between', marginBottom: 0 }]}>
                            <View style={[styles.flexRow, { alignItems: 'center' }]}>
                                <AntDesign name='tag' />
                                <Text style={{ marginLeft: calcScale(5) }}>Khuyến Mại</Text>
                            </View>
                            <TextInput
                                style={styles.inputVoucher}
                                placeholder='Nhập mã voucher'
                                value={this.state.voucherCode}
                                onChangeText={this.setVoucherCode}
                                onSubmitEditing={this.applyVoucherCode}
                            />
                        </View>
                    </View>

                    {/* Payment */}
                    <View style={styles.infoComponent}>
                        <Text>Hình thức thanh toán</Text>
                        <View style={[styles.flexRow, { marginTop: calcScale(10) }]}>
                            <TouchableOpacity style={[styles.itemPayment, { backgroundColor: '#e04e43' }]}>
                                <Text style={{ textAlign: 'center' }}>COD</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.itemPayment} onPress={this.notSupport}>
                                <Text style={{ textAlign: 'center' }}>Internet Banking</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.itemPayment} onPress={this.notSupport}>
                                <Text style={{ textAlign: 'center' }}>e-Wallet</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/* Confirm */}
                    <TouchableOpacity style={styles.buttonConfirm} onPress={this.submit}>
                        <Text style={{ textAlign: 'center' }}>Đặt Hàng</Text>
                    </TouchableOpacity>
                </ScrollView>
            )
        }
    }
}

const styles = StyleSheet.create({
    viewAll: {
        height: '100%',
        backgroundColor: mainBgr
    },
    header: {
        marginTop: calcScale(20),
        paddingHorizontal: calcScale(30)
    },
    infoComponent: {
        padding: calcScale(15),
        marginTop: calcScale(20),
        backgroundColor: '#fff'
    },
    flexRow: {
        flexDirection: 'row',
    },
    nameAndAddress: {
        marginLeft: calcScale(10),
        alignSelf: 'center'
    },
    textPrice: {
        fontStyle: 'italic',
        color: '#147fad'
    },
    flexRowAndSpace: {
        flexDirection: 'row',
        marginBottom: calcScale(15),
        alignItems: 'center'
    },
    seperatorBorder: {
        borderBottomColor: '#707070',
        paddingBottom: calcScale(5),
        borderBottomWidth: calcScale(1),
        marginBottom: calcScale(10)
    },
    inputVoucher: {
        backgroundColor: '#e0e0e0',
        width: calcScale(150),
        height: calcScale(45),
        borderRadius: calcScale(10),
        fontSize: calcScale(10)
    },
    timeComponent: {
        borderTopWidth: calcScale(1),
        borderTopColor: '#707070',
        paddingTop: calcScale(10),
        marginTop: calcScale(10),
        justifyContent: 'space-between',
        marginBottom: 0
    },
    fieldModal: {
        backgroundColor: '#fff',
        padding: calcScale(10),
        marginTop: calcScale(20),
        marginLeft: calcScale(20),
        flex: 7,
        borderColor: '#707070',
        borderWidth: calcScale(1)
    },
    itemPayment: {
        backgroundColor: '#faf4ee',
        borderRadius: calcScale(20),
        paddingVertical: calcScale(10),
        paddingHorizontal: calcScale(20),
        marginLeft: calcScale(20)
    },
    buttonConfirm: {
        padding: calcScale(15),
        backgroundColor: '#faed55',
        borderRadius: calcScale(20),
        width: calcScale(200),
        marginTop: calcScale(10),
        alignSelf: 'center',
        marginBottom: calcScale(30)
    }
})