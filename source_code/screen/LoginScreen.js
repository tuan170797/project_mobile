import * as React from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet, Alert, ActivityIndicator } from 'react-native'
import { calcScale, mainBgr } from './../constants/Constants'
import HeaderComponent from './../component/HeaderComponent'
import Session from './../constants/Session'
import AsyncStorage from '@react-native-community/async-storage'
import Modal from 'react-native-modal'
import AntDesign from 'react-native-vector-icons/AntDesign'
import MainAPI from '../api/MainAPI'

class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            usnForgot: '',
            isLoading: false,
            message: '',
            modalIsOpen: false
        }
    }

    setUsername = value => this.setState({ username: value })

    setPassword = value => this.setState({ password: value })

    setUsnForgot = value => this.setState({ usnForgot: value })

    doLogin = async (username, password) => {
        this.session = new Session()
        await this.session.login(username, password)
        const token = await AsyncStorage.getItem('Token')
        if (!token) {
            Alert.alert('Warning', 'Your username or password is incorrect')
        }
        else {
            this.props.navigation.navigate('UserScreen', { refresh: true })
        }
    }

    submit = async () => {
        this.setState({
            isLoading: true
        })
        const { usnForgot } = this.state
        this.api = new MainAPI()
        const resText = await this.api.forgotPassword(usnForgot)
        if (resText.status == 200) {
            this.setState({
                message: resText.message,
                isLoading: false
            })
        }
        else {
            this.setState({
                message: resText.message.username[0],
                isLoading: false
            })
        }
    }

    openModal = () => {
        this.setState({
            modalIsOpen: true
        })
    }

    renderModal = (modalIsOpen) => {
        return (
            <Modal isVisible={modalIsOpen}>
                <View style={{ backgroundColor: '#fff', padding: calcScale(20) }}>
                    <HeaderComponent
                        title='Forgot Password'
                        titleStyle={{ color: '#f6ad34' }}
                        backOnPress={() => this.setState({ modalIsOpen: false })}
                        onPressButtonRight={this.submit}
                        nameButtonRight='check'
                    />
                    {
                        this.state.isLoading ?
                            <ActivityIndicator color='#fff' />
                            : null
                    }
                    {
                        this.state.message ?
                            <Text style={{ color: 'red' }}>{this.state.message}</Text>
                            : null
                    }
                    <TextInput
                        placeholder='Your username'
                        style={[styles.inputField, { marginTop: calcScale(20) }]}
                        value={this.state.usnForgot}
                        onChangeText={this.setUsnForgot}
                    />
                </View>
            </Modal>
        )
    }

    render = () => {
        return (
            <View style={styles.viewAll}>
                {this.renderModal(this.state.modalIsOpen)}
                <HeaderComponent
                    title='Sign In'
                    style={{ marginTop: calcScale(20), marginBottom: calcScale(30) }}
                    backOnPress={() => this.props.navigation.navigate('UserScreen')}
                    buttonRightStyle={{ opacity: 0 }}
                    nameButtonRight='left'
                />
                <TextInput
                    style={[styles.textInput]}
                    value={this.state.username}
                    onChangeText={this.setUsername}
                    placeholder='Username'
                />
                <TextInput
                    style={styles.textInput}
                    value={this.state.password}
                    onChangeText={this.setPassword}
                    placeholder='Password'
                    secureTextEntry={true}
                />
                <TouchableOpacity onPress={this.openModal}>
                    <Text style={styles.textForgotPsw}>Forgot Password?</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.buttonLogin} onPress={() => this.doLogin(this.state.username, this.state.password)}>
                    <Text style={styles.textLogin}>Login</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default LoginScreen

const styles = StyleSheet.create({
    viewAll: {
        height: '100%',
        paddingHorizontal: calcScale(30),
        backgroundColor: mainBgr
    },
    textInput: {
        borderBottomColor: '#707070',
        borderBottomWidth: calcScale(1),
        marginTop: calcScale(30)
    },
    buttonLogin: {
        backgroundColor: '#f7ba56',
        padding: calcScale(15),
        borderRadius: calcScale(20),
        marginTop: calcScale(20)
    },
    textForgotPsw: {
        color: '#147fad',
        textAlign: 'right',
        marginTop: calcScale(20)
    },
    textLogin: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#fff'
    },
    inputField: {
        width: '100%',
        borderColor: '#1a1a1a',
        borderWidth: calcScale(1),
        borderRadius: calcScale(5),
        paddingHorizontal: calcScale(15)
    },
})