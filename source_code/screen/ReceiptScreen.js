import * as React from 'react'
import { View, StyleSheet, Text, ActivityIndicator } from 'react-native'
import { calcScale, mainBgr, currencyFormat } from './../constants/Constants'
import HeaderComponent from './../component/HeaderComponent'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Image from 'react-native-scalable-image'
import MainAPI from './../api/MainAPI'

export default class ReceiptScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {},
            isLoading: true,
        }
    }

    componentDidMount = async () => {
        let data = {}
        const prevScreen = this.props.navigation.getParam('prevScreen')
        switch (prevScreen) {
            case 'ConfirmOrder':
                data = this.props.navigation.getParam('data')
                break
            case 'OrderScreen':
                this.api = new MainAPI()
                const temp = await this.api.getDetailOrders(this.props.navigation.getParam('id'))
                data = temp.data
                break
        }
        this.setState({
            data: data,
            isLoading: false
        })
    }

    back = () => this.props.navigation.navigate('OrderScreen')

    render = () => {
        const { data, isLoading } = this.state
        if (isLoading) {
            return (
                <ActivityIndicator />
            )
        }
        else {
            return (
                <View style={styles.viewAll}>
                    <HeaderComponent
                        title='Receipt'
                        backOnPress={this.back}
                        buttonRightStyle={{ opacity: 0 }}
                        name='left'
                    />

                    <View style={styles.cardReceipt}>
                        <AntDesign name='checkcircle' size={calcScale(40)} color='#2bb673' style={{ alignSelf: 'center' }} />
                        {/* Date Time */}
                        <View style={[styles.flexRowAndSpace, { marginTop: calcScale(40) }]}>
                            <View>
                                <Text style={styles.textLabel}>DATE</Text>
                                <Text style={styles.textValue}>{data.date_order}</Text>
                            </View>
                            <View>
                                <Text style={[styles.textLabel, { textAlign: 'right' }]}>TIME</Text>
                                <Text style={styles.textValue}>{data.time_from} - {data.time_to}</Text>
                            </View>
                        </View>

                        {/* Order Number */}
                        <View style={{ marginTop: calcScale(40) }}>
                            <Text style={styles.textLabel}>Table Number</Text>
                            <Text style={styles.textValue}>{data.table_no}</Text>
                        </View>

                        {/* Amount */}
                        <View style={{ marginTop: calcScale(40) }}>
                            <Text style={styles.textLabel}>AMOUNT</Text>
                            <Text style={styles.textValue}>{currencyFormat(Number.parseFloat(data.total_price))}</Text>
                        </View>
                    </View>

                    <Image style={{ alignSelf: 'center' }} source={require('./../image/Thanks.jpg')} />
                </View>
            )
        }

    }
}

const styles = StyleSheet.create({
    viewAll: {
        backgroundColor: mainBgr,
        height: '100%',
        padding: calcScale(20),
    },
    cardReceipt: {
        alignSelf: 'center',
        width: calcScale(300),
        marginTop: calcScale(50),
        paddingHorizontal: calcScale(20),
        paddingVertical: calcScale(30),
        backgroundColor: '#fff',
        borderRadius: calcScale(20),
        shadowColor: '#1a1a1a',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    },
    flexRowAndSpace: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textLabel: {
        color: '#eab968',
    },
    textValue: {
        fontWeight: 'bold',
        marginTop: calcScale(10)
    },
    buttonCancel: {
        backgroundColor: '#e46a60',
        padding: calcScale(10),
        borderRadius: calcScale(20),
        width: calcScale(70),
        marginTop: calcScale(20),
        alignSelf: 'flex-end',
        marginRight: calcScale(35)
    }
})