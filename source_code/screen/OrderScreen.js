import * as React from 'react'
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native'
import { calcScale, mainBgr, currencyFormat } from './../constants/Constants'
import MainAPI from './../api/MainAPI'
import AsyncStorage from '@react-native-community/async-storage'

export default class OrderScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            listData: [],
            isLoading: true,
            tokenIsNull: true
        }
    }

    componentDidMount = async () => {
        const token = await AsyncStorage.getItem('Token')
        if (token) {
            this.api = new MainAPI()
            const listData = await this.api.getHistoryOrders()
            this.setState({
                tokenIsNull: false,
                listData: listData.data,
                isLoading: false
            })
        }
        else {
            this.setState({
                isLoading: false
            })
        }
        this.focusListener = this.props.navigation.addListener('didFocus', async () => {
            this.setState({ isLoading: true })
            const token = await AsyncStorage.getItem('Token')
            if (token) {
                this.api = new MainAPI()
                const listData = await this.api.getHistoryOrders()
                this.setState({
                    tokenIsNull: false,
                    listData: listData.data,
                    isLoading: false
                })
            }
            else {
                this.setState({
                    tokenIsNull: true,
                    isLoading: false,
                })
            }
        })
    }

    goToReceipt = (id) =>
        this.props.navigation.navigate('ReceiptScreen', { id: id, prevScreen: this.props.navigation.state.routeName })

    render = () => {
        const { listData, isLoading, tokenIsNull } = this.state
        if (isLoading) {
            return (
                <ActivityIndicator />
            )
        }
        else {
            if (tokenIsNull) {
                return (
                    <View style={styles.viewAll}>
                        {/* Title */}
                        <Text style={styles.header}>History</Text>
                        <Text style={{ marginTop: calcScale(20) }}>Please login to see your orders history</Text>
                    </View>
                )
            }
            else {
                if (listData.length == 0) {
                    return (
                        <View style={styles.viewAll}>
                            {/* Title */}
                            <Text style={styles.header}>History</Text>
                            {/* Show Order */}
                            <ScrollView showsVerticalScrollIndicator={false} style={{ marginVertical: calcScale(20) }}>
                                <Text>You don't have any order yet. Go to our menu and discover it ^.^</Text>
                            </ScrollView>
                        </View>
                    )
                }
                else {
                    return (
                        <View style={styles.viewAll}>
                            {/* Title */}
                            <Text style={styles.header}>History</Text>
                            {/* Show Order */}
                            <ScrollView showsVerticalScrollIndicator={false} style={{ marginVertical: calcScale(20) }}>
                                {
                                    listData.map(item => {
                                        return (
                                            <View style={styles.item}>
                                                <Text style={styles.textName}>ID: {item.orderId}</Text>
                                                <View style={styles.blockDateAndPrice}>
                                                    <Text style={styles.textDate}>{item.created_date}</Text>
                                                    <Text style={styles.textPrice}>{currencyFormat(Number.parseFloat(item.total_price))}</Text>
                                                </View>
                                                <TouchableOpacity style={styles.blockViewReceipt} onPress={() => this.goToReceipt(item.orderId)}>
                                                    <Text style={styles.textReceipt}>View Receipt</Text>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    })
                                }
                            </ScrollView>
                        </View>
                    )
                }
            }
        }
    }
}

const styles = StyleSheet.create({
    viewAll: {
        height: '100%',
        backgroundColor: mainBgr,
        paddingHorizontal: calcScale(20)
    },
    header: {
        marginTop: calcScale(30),
        fontWeight: 'bold',
        fontSize: calcScale(25),
        textAlign: 'center',
    },
    item: {
        padding: calcScale(15),
        backgroundColor: '#fff',
        marginTop: calcScale(20)
    },
    blockDateAndPrice: {
        marginTop: calcScale(5),
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: calcScale(1),
        borderBottomColor: '#707070',
        paddingBottom: calcScale(10)
    },
    blockViewReceipt: {
        marginTop: calcScale(10),
    },
    textName: {
        fontSize: calcScale(20)
    },
    textDate: {
        fontSize: calcScale(17),
        color: '#b7b5b5',
    },
    textPrice: {
        color: '#147fad',
        fontStyle: 'italic',
        fontSize: calcScale(17)
    },
    textReceipt: {
        color: '#f5a623',
        fontSize: calcScale(20),
        fontStyle: 'italic',
        textAlign: "center",
        fontWeight: 'bold'
    },

})