import AsyncStorage from '@react-native-community/async-storage';
export default class MainAPI {
    callAPI = async (action, data = {}, token = '') => {
        let responseText = '';
        try {
            let dataPost = JSON.stringify(data);
            console.log(dataPost, action)
            const response = await fetch('https://order-food-12.herokuapp.com/api/' + action, {
                method: 'POST',
                body: dataPost,
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }),

            });
            try {
                responseText = await response.json()
                return responseText
            }
            catch (e) {
                alert('something went wrong ');
                return []
            }

        } catch (error) {
            console.error(error);
        }
    }

    login = async (username, password) => {
        try {
            const action = 'auth/login'
            const data = { 'username': username, 'password': password }
            const resText = await this.callAPI(action, data)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getPopularProducts = async (category_id) => {
        try {
            const action = 'products-popular'
            const resText = await this.callAPI(action, { category_id: category_id })
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getNewProducts = async (category_id) => {
        try {
            const action = 'products-newest'
            const resText = await this.callAPI(action, { category_id: category_id })
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getPriceProducts = async (category_id) => {
        try {
            const action = 'products-price'
            const resText = await this.callAPI(action, { category_id: category_id })
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getDetailProducts = async (id) => {
        try {
            const action = 'detail-product'
            const resText = await this.callAPI(action, { id: id })
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getCategories = async () => {
        try {
            const action = 'categories'
            const resText = await this.callAPI(action)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    addToCart = async (product_id, amount) => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'add-to-cart'
            const data = { product_id: product_id, amount: amount }
            const resText = await this.callAPI(action, data, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getListCartByUser = async () => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'list-cart'
            const resText = await this.callAPI(action, {}, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    removeCart = async (cart_id) => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'remove-cart-item'
            const resText = await this.callAPI(action, { cart_id: cart_id }, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    insertOrder = async (dataObj = {}) => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'insert-order'
            const obj = {
                total_price: dataObj.total_price,
                status: dataObj.status,
                payment_id: dataObj.payment_id,
                date: dataObj.date,
                time_from: dataObj.time_from,
                time_to: dataObj.time_to,
                cart: dataObj.cart,
                voucher_code: dataObj.voucherCode
            }
            const resText = await this.callAPI(action, obj, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    register = async (dataObj = {}) => {
        try {
            const action = 'auth/register'
            const obj = {
                email: dataObj.email,
                username: dataObj.username,
                password: dataObj.password,
                fullname: dataObj.fullName,
                phone: dataObj.phone,
            }
            const resText = await this.callAPI(action, obj)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    confirmCodeRegister = async (username, confirm_code) => {
        try {
            const action = 'auth/confirm'
            const obj = {
                username: username,
                confirm_code: confirm_code
            }
            const resText = await this.callAPI(action, obj)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getListVouchers = async () => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'list-voucher'
            const resText = await this.callAPI(action, {}, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getHistoryOrders = async () => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'list-receipt'
            const resText = await this.callAPI(action, {}, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getDetailOrders = async (id) => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'detail-receipt'
            const resText = await this.callAPI(action, { order_id: id }, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    getUserInfo = async () => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'user-info'
            const resText = await this.callAPI(action, {}, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    submitEditInfo = async (dataObj = {}) => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'edit-profile'
            const obj = {
                fullname: dataObj.fullName,
                phone: dataObj.phone
            }
            const resText = await this.callAPI(action, obj, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    confirmChangePsw = async (dataObj) => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'auth/change-password'
            const obj = {
                password: dataObj.psw,
                new_password: dataObj.newPsw
            }
            const resText = await this.callAPI(action, obj, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    forgotPassword = async (username) => {
        try {
            const action = 'auth/forgot-password'
            const obj = {
                username: username,
            }
            const resText = await this.callAPI(action, obj)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    applyVoucher = async (voucher_code) => {
        try {
            const token = await AsyncStorage.getItem('Token')
            const action = 'apply-voucher'
            const obj = {
                voucher_code: voucher_code,
            }
            const resText = await this.callAPI(action, obj, token)
            return resText
        } catch (error) {
            console.log(error)
        }
    }

    search = async (keyword = '') => {
        try {
            const action = 'search'
            const obj = {
                keyword: keyword,
            }
            const resText = await this.callAPI(action, obj)
            return resText
        } catch (error) {
            console.log(error)
        }
    }
}

