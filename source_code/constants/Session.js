import AsyncStorage from '@react-native-community/async-storage';
import MainAPI from './../api/MainAPI'

let instance = null;


export default class Session {
    constructor() {
        if (!instance) {
            instance = this;
        }
        return instance;
    }

    login = async (usn, password) => {
        this.api = new MainAPI()
        const responseText = await this.api.login(usn, password)
        let token = ''
        let username = ''
        if (responseText.status === 'success') {
            token = responseText.token
            username = usn
            try {
                await AsyncStorage.setItem('Token', token)
                await AsyncStorage.setItem('Username', username)
            }
            catch (error) {

            }
        }
        else if (responseText.status === 'error') {
            //alert(responseText.message)
        }
    };

    logout = async () => {
        try {
            await AsyncStorage.removeItem('Token')
            await AsyncStorage.removeItem('Username')
        } catch (error) {
            // Error saving data
        }
    };

};
