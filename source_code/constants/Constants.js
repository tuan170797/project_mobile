import { Dimensions } from 'react-native'

export const calcScale = value => {
    const { width, height } = Dimensions.get('window')
    const baseWidth = 375
    const baseHeight = 778

    const scaleWidth = width / baseWidth
    const scaleHeight = height / baseHeight
    let scale = Math.min(scaleWidth, scaleHeight)
    scale = scale > 1 ? 1 : scale

    return Math.ceil(value * scale)
}

export const currencyFormat = num => {
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + 'đ'
}

export const mainBgr = '#faf4ee'
