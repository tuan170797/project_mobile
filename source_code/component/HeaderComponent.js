import * as React from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { calcScale } from './../constants/Constants'

export default class HeaderComponent extends React.Component {
    static defaultProps = {
        title: '',
        backOnPress: () => { },
        style: '',
        buttonRight: null
    }

    constructor(props) {
        super(props)

    }

    render = () => {
        const { title, backOnPress, style, titleStyle, backStyle, buttonRightStyle, nameButtonRight, onPressButtonRight } = this.props
        return (
            <View style={[style, styles.viewAll]}>
                <TouchableOpacity style={backStyle} onPress={backOnPress}>
                    <AntDesign color='#f6ad34' name='left' size={calcScale(30)} />
                </TouchableOpacity>

                <Text style={[styles.textTitle, titleStyle]}>{title}</Text>

                <TouchableOpacity style={buttonRightStyle} onPress={onPressButtonRight}>
                    <AntDesign color='#f6ad34' name={nameButtonRight} size={calcScale(30)} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewAll: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textTitle: {
        fontWeight: 'bold',
        fontSize: calcScale(25)
    }
})