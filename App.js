//Import library
import * as React from 'react'
import { StyleSheet } from 'react-native'
import { createAppContainer, SafeAreaView } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { calcScale, mainBgr } from './source_code/constants/Constants'
import CustomMenuComponent from './CustomMenuComponent'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import AnimatedSplash from 'react-native-animated-splash-screen'

//Screen
import HomeScreen from './source_code/screen/HomeScreen'
import UserScreen from './source_code/screen/UserScreen'
import LoginScreen from './source_code/screen/LoginScreen'
import RegisterScreen from './source_code/screen/RegisterScreen'
import DetailScreen from './source_code/screen/DetailScreen'
import OrderScreen from './source_code/screen/OrderScreen'
import ConfirmOrder from './source_code/screen/ConfirmOrder'
import ReceiptScreen from './source_code/screen/ReceiptScreen'
import VouchersScreen from './source_code/screen/VouchersScreen'
import SearchResult from './source_code/screen/SearchResult'

const styles = StyleSheet.create({
  labelStyle: {
    fontSize: 10,
    textAlign: 'center',
    textTransform: 'capitalize',
    margin: 0,
    padding: 0,
    justifyContent: 'center',
    fontFamily: 'Rubik-Regular',
  },
  tabStyle: {
    padding: 0,
    justifyContent: 'center',
    paddingTop: calcScale(10),
    textAlign: 'center',
    alignItems: 'center',
    backgroundColor: mainBgr,
    flex: 1,
  },
  indicatorStyle: {
    backgroundColor: '#f7ba56',
    height: calcScale(3),
    top: 0,
    bottom: 'auto'
  },
  barStyle: {
    height: calcScale(83),
    alignContent: 'center',
    borderWidth: 0,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
  },
  // iconStyle: {
  //   padding: 0,
  //   margin: 0,
  // },
  tabbarContainer: {
    width: '100%',
    flexDirection: 'row',
  },
})


const BottomStack = createBottomTabNavigator(
  {
    ['HomeScreen']: {
      screen: HomeScreen,
      navigationOptions: () => ({
        title: '',
        tabBarIcon: (event) => {
          return event.focused ?
            <FontAwesome5 size={calcScale(40)} name='home' color='#f7ba56' />
            :
            <FontAwesome5 size={calcScale(40)} name='home' color='#707070' />
        },
      }),
    },
    ['OrderScreen']: {
      screen: OrderScreen,
      navigationOptions: () => ({
        title: '',
        tabBarIcon: (event) => {
          return event.focused ?
            <FontAwesome5 size={calcScale(40)} name='clipboard-list' color='#f7ba56' />
            :
            <FontAwesome5 size={calcScale(40)} name='clipboard-list' color='#707070' />
        },
      }),
    },
    ['UserScreen']: {
      screen: UserScreen,
      navigationOptions: () => ({
        title: '',
        tabBarIcon: (event) => {
          return event.focused ?
            <FontAwesome5 size={calcScale(40)} name='user-circle' color='#f7ba56' />
            :
            <FontAwesome5 size={calcScale(40)} name='user-circle' color='#707070' />
        }
      }),
    },
  },
  {
    initialRouteName: 'HomeScreen',
    tabBarPosition: 'bottom',
    tabBarComponent: props => <CustomMenuComponent {...props} />,
    tabBarOptions: {
      inactiveTintColor: '#6D6E71',
      activeTintColor: '#f2fc68',
      indicatorStyle: styles.indicatorStyle,
      labelStyle: styles.labelStyle,
      tabStyle: styles.tabStyle,
      iconStyle: styles.iconStyle,
      style: styles.barStyle,
      keyboardHidesTabBar: true,
      showIcon: true,
      showLabel: true
    },
    lazy: true,
  }
)

const BottomContainer = createAppContainer(BottomStack)


const StackScreen = createStackNavigator(
  {
    ['BottomContainer']: {
      screen: BottomContainer,
      navigationOptions: {
        headerShown: false
      }
    },
    ['LoginScreen']: {
      screen: LoginScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    ['RegisterScreen']: {
      screen: RegisterScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    ['DetailScreen']: {
      screen: DetailScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    ['ConfirmOrder']: {
      screen: ConfirmOrder,
      navigationOptions: {
        headerShown: false
      }
    },
    ['ReceiptScreen']: {
      screen: ReceiptScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    ['VouchersScreen']: {
      screen: VouchersScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    ['SearchResult']: {
      screen: SearchResult,
      navigationOptions: {
        headerShown: false
      }
    },
  },
  {
    initialRouteName: 'BottomContainer'
  }
)
const AppContainer = createAppContainer(StackScreen)

class App extends React.Component {
  state = {
    isLoaded: false
  }

  componentDidMount = () => {
    this.timeOut = setTimeout(() => {
      this.setState({
        isLoaded: true
      })
    }, 1000)
  }

  render = () => {
    return (
      <SafeAreaView style={{ flex: 1, height: '100%', width: '100%' }}>
        <AnimatedSplash
          translucent={true}
          isLoaded={this.state.isLoaded}
          logoImage={require('./source_code/image/logoRestaurant.jpg')}
          backgroundColor='#d7fafe'
          logoHeight='90%'
          logoWidth='90%'
        >
          <AppContainer />
        </AnimatedSplash>
      </SafeAreaView>
    )
  }
}

export default App;


