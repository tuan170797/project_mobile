import * as React from 'react';
import { View, StyleSheet, Dimensions, TouchableOpacity, Animated } from 'react-native';
import { calcScale } from './source_code/constants/Constants';

const { height, width } = Dimensions.get("window");
export default class CustomMenuComponent extends React.PureComponent {
    static defaultProps = {
        activeTintColor: 'rgba(255, 255, 255, 1)',
        inactiveTintColor: 'rgba(255, 255, 255, 0.7)',
        showIcon: false,
        showLabel: true,
        upperCaseLabel: true,
        allowFontScaling: true,
    };
    constructor(props) {
        super(props);
        this.state = {
            menuOtherOpacity: new Animated.Value(0),
            isShowMenuOther: false
        }
    }
    _renderLabel = (route, focused, color, labelStyle) => {
        const {
            showLabel,
            upperCaseLabel, allowFontScaling, } = this.props;
        if (showLabel === false) {
            return null;
        }
        const label = this.props.getLabelText({ route });
        if (typeof label === 'string') {
            return (<Animated.Text style={[{ color }, labelStyle]} allowFontScaling={allowFontScaling}>{upperCaseLabel ? label.toUpperCase() : label}</Animated.Text>);
        }
        if (typeof label === 'function') {
            return label({ focused, tintColor: color });
        }
        return label;
    };
    _renderIcon = ({ route, focused, color }) => {
        const { renderIcon, showIcon, iconStyle } = this.props;
        if (showIcon === false) {
            return null;
        }
        return (<View style={iconStyle}>{renderIcon({ route, focused, tintColor: color })}</View>);
    };

    render() {
        const {
            style,
            activeTintColor,
            inactiveTintColor,
            tabStyle,
            onTabPress,
            getAccessibilityLabel,
            navigation,
            labelStyle,
            indicatorStyle
        } = this.props;
        const { routes, index: activeRouteIndex } = navigation.state;
        return (
            <View style={style}>
                {routes.map((route, routeIndex) => {
                    let isRouteActive = routeIndex === activeRouteIndex;
                    let tintColor = isRouteActive ? activeTintColor : inactiveTintColor;
                    return (
                        <TouchableOpacity
                            activeOpacity={0.95}
                            key={routeIndex}
                            style={tabStyle}
                            onPress={() => {
                                onTabPress({ route });
                                // this.props.navigation.navigate(route.key)
                            }}
                            accessibilityLabel={getAccessibilityLabel({ route })}
                        >
                            {
                                isRouteActive ? <View style={[indicatorStyle, styles.indicatorStyle]} /> : null
                            }

                            {this._renderIcon({ route, focused: isRouteActive, tintColor })}
                            {this._renderLabel(route, isRouteActive, tintColor, labelStyle)}
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    menuOtherContainer: {
        width: width,
        height: (height - calcScale(83)),
        position: 'absolute',
        bottom: '100%',
        left: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
    },
    menuOther: {
        width: calcScale(132),
        backgroundColor: 'transparent',
        position: 'absolute',
        bottom: 0,
        right: 0
    },
    menuOtherItem: {
        width: '100%',
        paddingBottom: calcScale(15),
        paddingTop: calcScale(15),
        paddingRight: calcScale(10),
        paddingLeft: calcScale(10),
        backgroundColor: '#1575FF',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        fontSize: calcScale(14),
        fontFamily: 'Rubik-Regular'
    },
    menuOtherItemActive: {
        backgroundColor: 'rgba(21, 117, 255, 0.5)'
    },
    menuOtherItemText: {
        color: '#fff',
        fontSize: calcScale(14),
        fontFamily: 'Rubik-Regular',
    },
    indicatorStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%'
    }
});
